def timeconversion(s):
    time = s.split(":")
    x = time.split(':')
    if time[-2:] == 'PM':
        if x[0] != '12':
            x[0] = str(int(x[0]) + 12)
    elif time[-2:] == 'AM':
        if x[0] == '12':
            x[0] = '00'
    hasil = ':'.join(time)
    return str(hasil[:-2])

def drawingBook(jmlhlm, tujuanhlm):
    x = jmlhlm//2
    s = tujuanhlm//2
    hasil = min(s,x-s)
    return str(hasil)

def kangoroo(x1,v1,x2,v2):
    max = 10000
    i = 0
    hasil = False

    while i<= max:
        i += 1
        x1 = x1 + v1
        x2 = x2 + v2
        if (x1 == x2):
            hasil = True

    if hasil:
        return 'YES'
    else:
        return 'NO'

def veryBigSum(ar):
    sum = 0
    i = 0
    while i < len(ar):
        sum += ar[i]
        i += 1

    return sum

def binarySearch(list, target):
    i = 0
    x = target
    list.sort()
    while i < len(list):
        i += 1
        if list[int(len(list) / 2)] == x:
            return 'Data Ditemukan ' + x
        if list[int(len(list) / 2)] < x:
            list = list[int(len(list) / 2):]
        elif list[int(len(list) / 2)] > x:
            list = list[:int(len(list) / 2)]
        if len(list) == 1 and list[0] != x:
            return 'Data tidak ditemukan ' + x

def compareTriplets(a,b):
    skor_a = 0
    skor_b = 0

    for i in range(len(a)):
        if a[i] < b[i]:
            skor_b += 1
        elif a[i] > b[i]:
            skor_a += 1

    hasil = [skor_a, skor_b]
    return hasil

def plusMinus(arr):
    positif = 0
    negatif = 0
    zero = 0

    for i in range(len(arr)):
        if arr[i] > 0:
            positif += 1
        elif arr[i] < 0:
            negatif += 1
        else:
            zero += 1

    hasil_p = positif / len(arr)
    hasil_n = negatif / len(arr)
    hasil_zero = zero / len(arr)
    return hasil_p,hasil_n, hasil_zero

def saveThePrisoner(n,m,s):
    bad = s + m - 1

    if (bad > n):
        if (bad % n == 0):
            return n
        else:
            return bad % n
    else:
        return bad

def howManyGames(p, d, m, s):
    jml_game = 0
    while s >= p and s >= 1:
        s = s - p
        p = p - d

        if p <= m:
           p = m

        jml_game+=1

    return jml_game


def merge_sort_asc(data):
    if len(data) <= 1:
        return data
    mid = int(len(data) // 2)
    left = merge_sort_asc(data[mid:])
    right = merge_sort_asc(data[:mid])
    return merge_sort2_asc(left,right)

def merge_sort2_asc(left,right):
    hasil = []
    l_index = 0
    r_index = 0
    while l_index < len(left) and r_index < len(right):
       if left[l_index] < right[r_index]:
          hasil.append(left[l_index])
          l_index += 1
       else:
          hasil.append(right[r_index])
          r_index += 1

    hasil.extend(left[l_index:])
    hasil.extend(right[r_index:])
    return hasil

def merge_sort_desc(data):
    if len(data) <= 1:
        return data
    mid = int(len(data) // 2)
    left = merge_sort_desc(data[mid:])
    right = merge_sort_desc(data[:mid])
    return merge_sort2_desc(left,right)

def merge_sort2_desc(left,right):
    hasil = []
    l_index = 0
    r_index = 0
    while l_index < len(left) and r_index < len(right):
       if left[l_index] < right[r_index]:
           hasil.append(right[r_index])
           r_index += 1
       else:
          hasil.append(left[l_index])
          l_index += 1

    hasil.extend(left[l_index:])
    hasil.extend(right[r_index:])
    return hasil


def betweentwoSet(a, b):
    hasil = 0
    max = b[0] + 1
    for i in range(a[-1], max):
        mark = True

        for array1 in a:
            if i % array1 != 0:
                mark = False

        if mark:
            for array2 in b:
                if array2 % i != 0:
                    mark = False

        if mark:
            hasil += 1

    return hasil


def diagonalDifference(arr):
    d1 = 0
    d2 = 0
    end = len(arr) -1
    for i in range(len(arr)):
        d1 += arr[i][i]
        d2 += arr[end-i][i]
    return abs(d1 - d2)

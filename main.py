from flask import Flask, request
import Function
app = Flask(__name__)

@app.route('/mergesort/', methods=['GET','POST'])
def merge_sort():
    data = request.get_json()['array']
    sort = request.headers.get('ASC')
    if sort == 'True':
        return {'Hasil Sort': Function.merge_sort_asc(data)}
    else:
        return {'Hasil sort': Function.merge_sort_desc(data)}

@app.route('/timeconversion/', methods=['POST'])
def timeconversion():
    time = request.get_json()['waktu']
    return {'Hasilnya' : Function.timeconversion(time)}

@app.route('/drawingbook/', methods=['POST'])
def drawingbook():
    req = request.get_json()
    jmlhlm = req['halaman']
    tujuanhlm = req['target']
    return {'Hasilnya' : Function.drawingBook(jmlhlm, tujuanhlm)}

@app.route('/numberline/', methods=['POST'])
def kangoroo():
    req = request.get_json()
    x1 = req['kang1']
    v1 = req['jump1']
    x2 = req['kang2']
    v2 = req['jump2']
    return {'Hasilnya': Function.kangoroo(x1,v1,x2,v2)}

@app.route('/verybigsum/', methods=['POST'])
def veryBigSum():
    ar = request.get_json()['array']
    return {'Hasilnya' : Function.veryBigSum(ar)}

@app.route('/binary_search/<int:target>', methods=['GET','POST'])
def binarySearch():
    list = request.get_json()['data']
    return{'Hasilnya' : Function.binarySearch(list, target)}

@app.route('/comparetriplets/', methods=['POST'])
def compareTriplets():
    req = request.get_json()
    a = req['data1']
    b = req['data2']
    return{'Hasilnya' : Function.compareTriplets(a,b)}

@app.route('/plusminus/', methods=['POST'])
def plusMinus():
    arr = request.get_json()['data']
    return {'Hasilnya' : Function.plusMinus(arr)}

@app.route('/saveprisoner/', methods=['POST'])
def saveThePrisoner():
    req = request.get_json()
    n = req['jml_prisoner']
    m = req['jml_candy']
    s = req['f_point']
    return {'Hasilnya' : Function.saveThePrisoner(n,m,s)}

@app.route('/hallowensale/', methods=['POST'])
def hallowenSale():
    req = request.get_json()
    p = req['harga']
    d = req['diskon']
    m = req['harga_min']
    s = req['uang']
    return {'Hasilnya' : Function.howManyGames(p, d, m, s)}

@app.route('/betweentwoSet/', methods=['POST'])
def betweentwoset():
    req = request.get_json()
    a = req['arr1']
    b = req['arr2']
    return {'Hasilnya' : Function.betweentwoSet(a,b)}

@app.route('/diagonal_difference/', methods=['POS'])
    arr = request.get_json()['array']
    return {'Hasilnya' : Function.diagonalDifference(arr)}

@app.route('/')
    print('Hello World')